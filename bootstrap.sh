#!/bin/bash
export KUBECONFIG="$HOME/.kube/config"

echo "Installing Nginx Ingress..."
kubectl create -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/cloud/deploy.yaml
kubectl wait --namespace ingress-nginx \
--for=condition=ready pod \
--selector=app.kubernetes.io/component=controller \
--timeout=160s

echo "Installing Cert-Manager..."
kubectl create -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
kubectl wait --namespace cert-manager \
--for=condition=ready pod \
--selector=app.kubernetes.io/component=controller \
--timeout=160s