package main

import (
	"fmt"
	"kubernello/internal/container"
	"kubernello/pkg"
	"os"
	"strings"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type Config struct {
	SSHUser         string
	SSHUserPassword string
	SSHPassword     string
	SSHost          string
	SSHKey          string

	ClusterName    string
	ClusterContext string
	ClusterKind    string

	K3sConfigPath  string
	KubeConfigPath string
	KubeConfig     string

	LogLevel string
}

var (
	config        Config
	envPrefix     = "KUBERNELLO"
	userDir, _    = homedir.Dir()
	SSHDir        = userDir + "/.ssh/id_ed25519"
	kubeConfigDir = userDir + "/.kube/config"
	rootCmd       = &cobra.Command{
		RunE: run,
		Use:  "kubernello",
	}
)

func init() {
	v := startConfig()

	rootCmd.Flags().StringVar(&config.SSHUser, "SSHUser", "", "sets the username of ssh user")
	rootCmd.Flags().StringVar(&config.SSHost, "SSHost", "", "sets the host machine ip to access with ssh")
	rootCmd.Flags().StringVar(&config.SSHPassword, "SSHPassword", "", "sets the ssh password")
	rootCmd.Flags().StringVar(&config.SSHUserPassword, "SSHUserPassword", "", "sets the ssh user password")
	rootCmd.Flags().StringVar(&config.SSHKey, "SSHKey", SSHDir, "sets the ssh key path to use")
	rootCmd.Flags().StringVar(&config.LogLevel, "loglevel", "Debug", "custom loglevel (default is in ..) ")
	rootCmd.Flags().StringVar(&config.KubeConfigPath, "kubeConfigPath", kubeConfigDir, "local kube config path")

	rootCmd.Flags().StringVar(&config.ClusterKind, "kind", "k3s", "sets the type of cluster to create")
	rootCmd.Flags().StringVar(&config.ClusterName, "name", "k3s", "sets the type of cluster to create")
	rootCmd.Flags().StringVar(&config.ClusterContext, "context", "k3s", "sets the type of cluster to create")

	bindFlags(rootCmd, v)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func bindFlags(cmd *cobra.Command, v *viper.Viper) {
	cmd.Flags().VisitAll(
		func(f *pflag.Flag) {
			// Environment variables can't have dashes in them, so bind them to their equivalent
			// keys with underscores, e.g. --favorite-color to STING_FAVORITE_COLOR
			if strings.Contains(f.Name, "-") {
				envVarSuffix := strings.ToUpper(strings.ReplaceAll(f.Name, "-", "_"))
				err := v.BindEnv(f.Name, fmt.Sprintf("%s_%s", envPrefix, envVarSuffix))
				if err != nil {
					log.Fatal(err)
					os.Exit(-1)
				}
			}
			// Apply the viper config value to the flag when the flag is not set and viper has a value
			if !f.Changed && v.IsSet(f.Name) {
				val := v.Get(f.Name)
				err := cmd.Flags().Set(f.Name, fmt.Sprintf("%v", val))
				if err != nil {
					log.Fatal(err)
					os.Exit(-1)
				}
			}
		},
	)
}

func startConfig() *viper.Viper {
	v := viper.New()

	fmt.Printf("config file is %+v", config)

	if config.K3sConfigPath != "" {
		// Use config file from the flag.
		v.SetConfigFile(config.K3sConfigPath)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		fmt.Printf("home is %s", home)
		cobra.CheckErr(err)
		// Search config in home directory with name (without extension).
		v.AddConfigPath(home)
		v.SetConfigType("yaml")
		v.SetConfigName(".k3s")
	}

	fmt.Printf("\n read file is %v \n", v.ConfigFileUsed())
	// Attempt to read the config file, gracefully ignoring errors
	// caused by a config file not being found. Return an error
	// if we cannot parse the config file.
	if err := v.ReadInConfig(); err != nil {
		// It's okay if there isn't a config file
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			log.Info(err)
		}
	}

	v.SetEnvPrefix(envPrefix)
	// Bind to environment variables
	// Works great for simple config names, but needs help for names
	// like --favorite-color which we fix in the bindFlags function
	v.AutomaticEnv()

	fmt.Println(v.AllKeys())

	return v
}

func run(cmd *cobra.Command, args []string) error {
	lvl, err := log.ParseLevel(config.LogLevel)

	if err != nil {
		log.WithField("log-level", config.LogLevel)

		return err
	}

	log.SetLevel(lvl)
	log.WithField("log-level", config.LogLevel).Debug("log level configured")

	k := container.NewContainer(
		container.Parameters{
			SSHUser:         container.SSHUser(pkg.NewNEString(config.SSHUser, "SSHUser")),
			SSHost:          container.SSHost(pkg.NewNEString(config.SSHost, "SSHost")),
			SSHKey:          container.SSHKey(pkg.NewNEString(config.SSHKey, "SSHKey")),
			SSHUserPassword: container.SSHUserPassword(pkg.NewNEString(config.SSHUserPassword, "SSHUserPassword")),
			SSHPassword:     container.SSHPassword(pkg.NewNEString(config.SSHPassword, "SSHPassword")),

			ClusterKind:    container.ClusterKind(pkg.NewNEString(config.ClusterKind, "ClusterKind")),
			ClusterName:    container.ClusterName(pkg.NewNEString(config.ClusterName, "ClusterName")),
			ClusterContext: container.ClusterContext(pkg.NewNEString(config.ClusterContext, "ClusterContext")),

			LogLevel: lvl,
		})

	err = k.Kubernello().CreateCluster(config.KubeConfigPath)
	err = k.Kubernello().DeployBaseResources()
	// err = k.Kubernello().DestroyCluster()
	if err != nil {
		log.Error(err.Error())
	}

	return nil
}
