package pkg

import (
	"fmt"
)

func NewNEString(value string, subj string) NEString {
	assertNotEmpty(value, subj)

	return NEString{stringer: stringer{string: value}}
}

type NEString struct {
	stringer
}

type stringer struct {
	string
	fmt.Stringer
}

func (s stringer) String() string {
	return s.string
}
