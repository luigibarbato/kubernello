package pkg

type ContainerFactory interface {
	Create(parameters interface{}) Container
}

type Container interface {
	Terminate()
}
