package pkg

import (
	"fmt"
)

func assertNotEmpty(value, subj string) {
	if value == "" {
		panic(fmt.Sprintf("%s: the value is empty", subj))
	}
}
