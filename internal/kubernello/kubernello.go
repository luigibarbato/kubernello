package kubernello

import (
	"kubernello/internal/cluster"
	"kubernello/internal/kubeconfig"

	"github.com/melbahja/goph"
	log "github.com/sirupsen/logrus"
)

type Kubernello struct {
	logger         *log.Logger
	sshClient      *goph.Client
	clusterFactory cluster.ClusterFactory
}

func NewKubernello(
	logger *log.Logger,
	sshClient *goph.Client,
	clusterFactory cluster.ClusterFactory,
) *Kubernello {
	return &Kubernello{
		logger:         logger,
		sshClient:      sshClient,
		clusterFactory: clusterFactory,
	}
}

func (k *Kubernello) CreateCluster(kubeConfigPath string) error {
	config, err := k.clusterFactory.CreateCluster(k.sshClient, "")
	kubeconfig.InstallNewContext(config, kubeConfigPath, k.sshClient.Config.Addr, "")

	return err
}

func (k *Kubernello) DestroyCluster() error {
	err := k.clusterFactory.DestroyCluster(k.sshClient)

	return err
}

func (k *Kubernello) DeployBaseResources() error {
	err := cluster.DeployBaseResources()

	return err
}
