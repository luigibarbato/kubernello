package container

import (
	"kubernello/internal/cluster"
	"kubernello/internal/kubernello"

	"github.com/melbahja/goph"
	log "github.com/sirupsen/logrus"
)

type Parameters struct {
	SSHUser         SSHUser
	SSHost          SSHost
	SSHKey          SSHKey
	SSHUserPassword SSHUserPassword
	SSHPassword     SSHPassword

	ClusterName    ClusterName
	ClusterKind    ClusterKind
	ClusterContext ClusterContext

	LogLevel log.Level
}

type services struct {
	sshClient      *goph.Client
	kubernello     *kubernello.Kubernello
	logger         *log.Logger
	clusterFactory cluster.ClusterFactory
}

type Container struct {
	Parameters
	services
}

func NewContainer(params Parameters) *Container {
	return &Container{
		Parameters: params,
	}
}

func (c *Container) Logger() *log.Logger {
	if c.logger == nil {
		logger := log.New()
		logger.SetLevel(c.LogLevel)

		c.logger = logger
	}

	return c.logger
}

func (c *Container) ClusterFactory() cluster.ClusterFactory {
	if c.clusterFactory == nil {
		cp := cluster.NewClusterFactoryParam(c.ClusterKind.String())
		c.clusterFactory = cp.ClusterFactory()
	}

	return c.clusterFactory
}

func (c *Container) SSHClient() *goph.Client {
	if c.sshClient == nil {

		auth, err := goph.Key(c.SSHKey.String(), c.SSHPassword.String())
		if err != nil {
			log.Fatal(err)
		}

		client, err := goph.New(c.SSHUser.String(), c.SSHost.String(), auth)
		if err != nil {
			log.Fatal(err)
		}

		c.sshClient = client
	}

	log.Info("SSH is correctly configured.")

	return c.sshClient
}

func (c *Container) Kubernello() *kubernello.Kubernello {
	if c.kubernello == nil {
		c.kubernello = kubernello.NewKubernello(
			c.Logger(),
			c.SSHClient(),
			c.ClusterFactory(),
		)
	}

	return c.kubernello
}

func (c *Container) Terminate() error {
	if c.sshClient != nil {
		return c.sshClient.Close()
	}

	return nil
}
