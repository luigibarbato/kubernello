package container

import dix "kubernello/pkg"

type SSHUser dix.NEString
type SSHost dix.NEString
type SSHKey dix.NEString
type SSHUserPassword dix.NEString
type SSHPassword dix.NEString
type ClusterName dix.NEString
type ClusterKind dix.NEString
type ClusterContext dix.NEString
