package cluster

import "os/exec"

var (
	bootstrap string
)

func DeployBaseResources() error {
	bootstrap = "../../bootstrap.sh"
	cmd := exec.Command("/bin/sh", bootstrap)

	return cmd.Run()
}
