package cluster

import (
	"log"

	"github.com/melbahja/goph"
)

type ClusterFactory interface {
	CreateCluster(sshClient *goph.Client, options string) (string, error)
	DestroyCluster(sshClient *goph.Client) error
}

type ClusterFactoryParam struct {
	ClusterName    string
	ClusterContext string
	ClusterKind    string
}

func NewClusterFactoryParam(kind string) ClusterFactoryParam {
	return ClusterFactoryParam{ClusterKind: kind}

}

func (c *ClusterFactoryParam) ClusterFactory() ClusterFactory {
	switch c.ClusterKind {
	case "k3s":
		k3s := NewK3sFactory(c.ClusterName, c.ClusterContext)
		return k3s
	default:
		log.Fatalf("Unknow cluster kind: %s", c.ClusterKind)

		return nil
	}
}
