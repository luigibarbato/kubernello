package cluster

import (
	"github.com/melbahja/goph"
	log "github.com/sirupsen/logrus"
)

type K3s struct {
	name    string
	context string
}

func NewK3sFactory(name, context string) *K3s {
	return &K3s{
		name:    name,
		context: context,
	}

}

func (k *K3s) CreateCluster(sshClient *goph.Client, options string) (string, error) {
	log.Info("Installing K3s Cluster...")
	_, err := sshClient.Run(`curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_EXEC="--disable=traefik" sh -`)
	config, err := sshClient.Run("sudo -i | cat /etc/rancher/k3s/k3s.yaml")

	return string(config), err
}

func (k *K3s) DestroyCluster(sshClient *goph.Client) error {
	log.Info("Destroying K3s Cluster...")
	_, err := sshClient.Run(`sudo -i | exec "/usr/local/bin/k3s-uninstall.sh"`)

	return err
}
