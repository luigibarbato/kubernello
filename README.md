# Kubernello
This is my personal tool to bootstrap a Kubernetes Cluster on Cloud.
## How to Work
Once entered every information needed, The tool will access to external machine through SSH, Install the Kubernetes Cluster choosen type (only K3s available for now) and install basic resources like Nginx Ingress Controller and Cert-Manager.
## Usage
````Flags:
      --SSHKey string            sets the ssh key path to use (default "/home/user/.ssh/id_ed25519")
      --SSHPassword string       sets the ssh password
      --SSHUser string           sets the username of ssh user
      --SSHUserPassword string   sets the ssh user password
      --SSHost string            sets the host machine ip to access with ssh
      --context string           sets the type of cluster to create (default "k3s")
      --kind string              sets the type of cluster to create (default "k3s")
      --kubeConfigPath string    local kube config path (default "/home/user/.kube/config")
      --loglevel string          custom loglevel (default is in ..)  (default "Debug")
      --name string              sets the type of cluster to create (default "k3s")
      -h, --help                     help for kubernello
````
